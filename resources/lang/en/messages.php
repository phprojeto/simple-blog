<?php

return [
	'button_delete' => 'Delete',
	'button_edit' => 'Edit',
    'button_view' => 'View',
    'title_actions' => 'Actions',
    'title_created_at' => 'Created At',
    'title_created_by' => 'Created By',
    'title_new' => 'New post',
    'title_subtitle' => 'Subtitle',
    'title_title' => 'Title',
];
