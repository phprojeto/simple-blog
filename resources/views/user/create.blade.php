@extends('user.baseuser')

@section('title' , 'create user')

@section('container')
	<h2>Create user</h2>
	<form action="/user/add" method="post">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="text" name="name" placeholder="name user">
		<input type="email" name="email" placeholder="email user">
		<input type="password" name="password" placeholder="Password">
		<input type="password" name="password_confirmation" placeholder="Repeat Password">
		<input type="submit" value="Submit">
	</form>

@endsection