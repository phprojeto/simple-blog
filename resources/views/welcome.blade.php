@extends('layouts.app')

@section('image')

class="masthead" style="background-image: url('https://blackrockdigital.github.io/startbootstrap-clean-blog/img/home-bg.jpg')"

@endsection

@section('heading')

Clean Blog

@endsection

@section('subheading')

A Blog Theme by Start Bootstrap

@endsection

@section('content')

<div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        
        @for($i = 0; $i < count($posts); $i++)
        <div class="post-preview">
          <a href="{{ route('blog.post', $posts[$i]->url) }}">
            <h2 class="post-title">
              {{ $posts[$i]->title }}
            </h2>
            <h3 class="post-subtitle">
              {{ $posts[$i]->subtitle }}
            </h3>
          </a>
          <p class="post-meta">Posted by
            <a href="#">{{ $posts[$i]->posted_by }}</a>
            on September 24, 2019</p>
        </div>
        <hr>
        @endfor
        <!-- Pager -->
        <div class="clearfix">
          <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
        </div>
      </div>
    </div>

@endsection