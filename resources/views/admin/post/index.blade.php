@extends('admin.app')

@section('content')
<div class="container">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel-heading">
        	<h3 class="panel-title"> New Post </h3>
        </div>

    	<div class="col-md-12">
	        <a href="{{ route('admin.post.create') }}" class="btn btn-primary pull-right h2">
	        	{{ __('messages.title_new') }}
	    	</a>
		</div>
	</div> 
	<div class="panel-body">
		<table class="table table-striped">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">{{ __('messages.title_created_by') }}</th>
					<th scope="col">{{ __('messages.title_title') }}</th>
					<th scope="col">{{ __('messages.title_subtitle') }}</th>
					<th scope="col">{{ __('messages.title_created_at') }}</th>
					<th scope="col">{{ __('messages.title_actions') }}</th>
				</tr>
			</thead>
			<tbody>
				@foreach($posts as $post)
			    	<tr>
			    		<?php //dd($post) ?>
						<td>{{ $post->id }}</td>
						<td>{{ $post->created_by }}</td>
						<td>{{ $post->title }}</td>
						<td>{{ $post->subtitle }}</td>
						<td>{{ $post->created_at }}</td>
						<td class="actions">
							<a class="btn btn-success btn-xs" href="">{{ __('messages.button_view') }}</a>
							<a class="btn btn-warning btn-xs" href="{{ route('admin.post.edit', ['id' => $post->id]) }}">
								{{ __('messages.button_edit') }}
							</a>
							
							<form method="POST" id="delete-form-{{ $post->id }}" action="{{ route('admin.post.delete', ['id' => $post->id]) }}" style="display: none;">
								{{ csrf_field() }}
								{{ method_field('delete') }}

							</form>
								
								<button onclick="hasDelete('{{ $post->id }}');" class="btn btn-danger btn-xs">{{ __('messages.button_delete') }}</button>
						</td>
			    	</tr>
		    	@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
@section('js')
	<script type="text/javascript">
		function hasDelete(id) {
			event.preventDefault();
			if (confirm('Are you Sure, you went to delete this?')) {
				document.getElementById('delete-form-' + id).submit();
			}
		}
	</script>
@endsection

