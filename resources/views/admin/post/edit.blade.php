@extends('admin.app')

@section('content')
<div class="container">
    <div class="panel-heading">
       <h3 class="panel-title"> Add Post </h3>
    </div>
        <div class="panel-body">
<form action="{{ route('admin.post.update', $post->id) }}" method="POST">
        {{ csrf_field() }}
    <fieldset>
    <div class="form-group">
    <label for="formGroupExampleInput">Title</label>
    <input type="text" class="form-control" id="formGroupExampleInput" value="{{ $post->title }}" name="title" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput">Subtitle</label>
    <input type="text" class="form-control" id="formGroupExampleInput" value="{{ $post->subtitle }}" name="subtitle" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Text</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" name="text" rows="3">{{ $post->text }}</textarea>
  </div>
  <div class="form-group">
    <label for="url">Url</label>
    <input type="text" class="form-control" id="url" readonly name="url" value="{{ $post->url }}">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" value="{{ $post->image }}" name="image">
  </div>
  <button id="submit" name="submit" class="btn btn-primary" value="submit">Submit</button>
  <a href="{{ route('admin.post') }}" id="cancel" name="cancel" class="btn btn-default">Cancel</a>
</fieldset>
</form>
        </div>
</div>

@endsection