@extends('admin.app')

@section('content')
<div class="container">
    <div class="panel-heading">
       <h3 class="panel-title"> Add Post </h3>
    </div>
        <div class="panel-body">
<form action="{{ route('admin.post.store') }}" method="POST">
        {{ csrf_field() }}
    <fieldset>
    <div class="form-group">
    <label for="formGroupExampleInput">Title</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="title" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput">Subtitle</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="subtitle" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Text</label>
    <textarea class="form-control" id="exampleFormControlTextarea1" name="text" rows="3"></textarea>
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput">Created By</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="created_by" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput">Url</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="url" placeholder="Input exemplo">
  </div>
  <div class="form-group">
    <label for="exampleFormControlFile1">Image</label>
    <input type="file" class="form-control-file" id="exampleFormControlFile1" name="image">
  </div>
  <input class="btn btn-primary" type="submit" value="Submit">
</fieldset>
</form>
      </div>
</div>

@endsection