  <!-- Page Header -->
  <header @yield('image')>
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>@yield('heading')</h1>
            <span class="subheading">@yield('subheading')</span>
            <span class="meta">@yield('posted')</span>
          </div>
        </div>
      </div>
    </div>
  </header>