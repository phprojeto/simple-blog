<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function(){
	Route::get('/', ['uses' => 'BlogController@index', 'as' => 'blog.home']);
	Route::get('/{url}', ['uses' => 'BlogController@post', 'as' => 'blog.post' ]);
	Route::get('/about', ['uses' => 'BlogController@about', 'as' => 'blog.about']);
	Route::get('/contact', ['uses' => 'BlogController@contact', 'as' => 'blog.contact']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin'], function(){
		Route::get('/post', ['uses' => 'PostController@index', 'as' => 'admin.post']);
		Route::get('/post/create', ['uses' => 'PostController@create', 'as' => 'admin.post.create']);
		Route::post('/post/store', ['uses' => 'PostController@store', 'as' => 'admin.post.store']);
		Route::get('/post/{id}/edit', ['uses' => 'PostController@edit', 'as' => 'admin.post.edit']);
		Route::post('/post/{id}/update', ['uses' => 'PostController@update', 'as' => 'admin.post.update']);
		Route::delete('post/{id}/delete', ['uses' => 'PostController@delete', 'as' => 'admin.post.delete']);
});