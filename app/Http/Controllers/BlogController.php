<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = DB::table('posts')
        ->join('users', 'users.id',  '=' , 'posts.created_by')
        ->select('posts.*', 'users.name as posted_by')
        ->get();

        return view('welcome', [
            'posts' => $posts
        ]);
    }

    public function post($url)
    {
        $post = DB::table('posts')
       ->select('posts.*', 'users.name as posted_by')
       ->join('users', 'users.id',  '=' , 'posts.created_by')
       ->where('url', $url)->first();

        return view('post', [
            'post' => $post 
        ]);
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }
}
