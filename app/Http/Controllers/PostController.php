<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index()
	{
		//$posts = DB::table('posts')->select('*')->get();
		$posts = Post::get();
		
		return view('admin.post.index', [
			'posts' => $posts
		]);
	}

	public function create()
	{

		return view('admin.post.add');
	}

	public function store(Request $request)
	{
		//dd($request->all());
		//dd($request->get('created_by'));

		$post = new Post;
		$post->title = $request->input('title');
		$post->subtitle = $request->input('subtitle');
		$post->text = $request->input('text');
		$post->created_by = Auth::id();
		$post->url = $slug = str_slug($post->title, '-');
		$post->image = $request->input('image');
		$post->save();
		return redirect()->route('admin.post')->with('sucess');
	}

	public function edit($id)
	{
		try {
			$post = Post::find($id);
			if(empty($post)){
				throw new \App\Exceptions\PostNotFound();
			}
		} catch(\App\Exceptions\PostNotFound $e) {
			return redirect()->route('admin.post');
		}

		return view('admin/post/edit', [
			'post' => $post
		]);
	}

	public function update(Request $request, $id)
	{
		$post = Post::find($id);
		$post->title = $request->input('title');
		$post->subtitle = $request->input('subtitle');
		$post->text = $request->input('text');
		$post->created_by = Auth::id();
		$post->url = $slug = str_slug($post->title, '-');
		$post->image = $request->input('image');
		$post->save();
		return redirect()->route('admin.post')->with('sucess');
	}

	public function delete($id)
	{
		Post::find($id)->delete();
			return redirect()->route('admin.post')->with('sucessMsg', 'Post Sucessfully Delete');
	}
}
