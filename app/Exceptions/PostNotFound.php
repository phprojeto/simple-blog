<?php 

namespace App\Exceptions;

class PostNotFound extends \Exception
{
	public function errorMessage()
	{
		return 'Post not Found';
	}
}

